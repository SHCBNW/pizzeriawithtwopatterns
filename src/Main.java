import commandPattern.*;
import factoryMethodPattern.pizzen.Salamipizza;
import factoryMethodPattern.pizzen.Thunfischpizza;

public class Main {
    public static void main(String[] args) {

        WarteSchlangeInvoker invoker = new WarteSchlangeInvoker();

        KellnerClient kellner1 = new KellnerClient("Tim");
        KellnerClient kellner2 = new KellnerClient("Alex");
        KochReceiver koch1 = new KochReceiver("Paul");
        KochReceiver koch2 = new KochReceiver("Max");

        kellner1.bestellungAbgeben(new BestellungZumHierEssenCommand(koch1, new Salamipizza()));
        kellner1.execute(invoker);

        kellner1.bestellungAbgeben(new BestellungZumHierEssenCommand(koch1, new Salamipizza()));
        kellner1.execute(invoker);

        kellner1.bestellungAbgeben(new BestellungZumHierEssenCommand(koch1, new Salamipizza()));
        kellner1.execute(invoker);

        kellner2.bestellungAbgeben(new BestellungZumMitnehmenCommand(koch2, new Thunfischpizza()));
        kellner2.execute(invoker);

        invoker.execNextCommand();
        invoker.execNextCommand();
        invoker.execNextCommand();
        invoker.execNextCommand();
        invoker.execNextCommand();
    }
}
