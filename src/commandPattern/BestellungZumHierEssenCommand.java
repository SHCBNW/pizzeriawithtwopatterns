package commandPattern;

import factoryMethodPattern.pizzen.Pizza;

public class BestellungZumHierEssenCommand implements Command {

    private KochReceiver kochReceiver;
    private Pizza pizza;

    @SuppressWarnings("SpellCheckingInspection")
    public BestellungZumHierEssenCommand(KochReceiver kochReceiver, Pizza pizza) {
        this.kochReceiver = kochReceiver;
        this.pizza = pizza;
    }


    @Override
    public void execute() {
        this.kochReceiver.performCommand(this);
    }

    @Override
    public void action() {
        pizza.vorbereiten();
        pizza.backen();
        pizza.schneiden();
    }
}
