package commandPattern;

public class KellnerClient {

    private String name;
    private Command command;

    public KellnerClient(String name) {
        this.name = name;
    }

    public void bestellungAbgeben(Command command) {
        System.out.println("Kellner " + name + " nimmt Bestellung auf");
        this.command = command;
    }

    public void execute(WarteSchlangeInvoker invoker) {
        invoker.addCommand(this.command);
    }

}
