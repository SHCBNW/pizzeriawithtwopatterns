package commandPattern;

public class KochReceiver {
    private String name;

    public KochReceiver(String name) {
        this.name = name;
    }

    public void performCommand(Command command) {
        System.out.println("Koch " + name + " startet Kommando.");
        command.action();
    }
}
