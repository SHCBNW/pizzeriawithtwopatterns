package commandPattern;

import java.util.ArrayList;
import java.util.List;

public class WarteSchlangeInvoker {

    private List<Command> bestellungen = new ArrayList<>();

    public WarteSchlangeInvoker() {

    }

    public void addCommand(Command command) {
        this.bestellungen.add(command);
    }

    public void execNextCommand() {
        if (bestellungen.size() > 0) {
            bestellungen.get(0).execute();
            bestellungen.remove(0);
        } else
            System.out.println("Keine Pizza in der Warteschlange");
    }

    public void undo() {
        bestellungen.remove(getBestellungenSize() - 1);
    }

    public int getBestellungenSize() {
        return bestellungen.size();
    }
}

