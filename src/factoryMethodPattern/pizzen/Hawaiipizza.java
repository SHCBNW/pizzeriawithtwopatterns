package factoryMethodPattern.pizzen;

import factoryMethodPattern.zutaten.Ananas;
import factoryMethodPattern.zutaten.Schinken;

public class Hawaiipizza extends Pizza {
    public Hawaiipizza() {
        super.name = "Hawaiipizza";
        super.preis = 7.50;
        super.zutaten.add(new Schinken());
        super.zutaten.add(new Ananas());
    }
}
