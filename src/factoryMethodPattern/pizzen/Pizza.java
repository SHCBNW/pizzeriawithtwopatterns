package factoryMethodPattern.pizzen;

import factoryMethodPattern.zutaten.Zutat;

import java.util.ArrayList;
import java.util.List;

public abstract class Pizza {
    String name;
    double preis;
    List<Zutat> zutaten = new ArrayList<>();

    public String getName() {
        return this.name;
    }

    // Zubereitung
    public void vorbereiten() {
        System.out.println("Pizza wird jetzt vorbereitet. Zutaten werden zusammengesucht.");
    }

    public void backen() {
        System.out.println("Pizza wird jetzt gebacken.");
    }


    // Optional
    public void schneiden() {
        System.out.println("Pizza wird jetzt geschnitten.");
    }

    public void einpacken() {
        System.out.println("Pizza wird nun eingepackt");
    }

    // Sonstige Methoden
    public void addZusatzZutat(Zutat zutat) {
        this.zutaten.add(zutat);
        preis += zutat.getPreis();
    }

    // Getter
    public double getPreis() {
        return this.preis;
    }

    public List<Zutat> getZutaten() {
        return this.zutaten;
    }


}
