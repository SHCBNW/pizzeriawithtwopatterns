package factoryMethodPattern.pizzen;

import factoryMethodPattern.zutaten.Salami;

public class Salamipizza extends Pizza {
    public Salamipizza() {
        super.name = "Salamipizza";
        super.preis = 6.50;
        super.zutaten.add(new Salami());
    }
}
