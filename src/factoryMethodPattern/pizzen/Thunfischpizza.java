package factoryMethodPattern.pizzen;

import factoryMethodPattern.zutaten.Thunfisch;

public class Thunfischpizza extends Pizza {
    public Thunfischpizza() {
        super.name = "Salamipizza";
        super.preis = 6.50;
        super.zutaten.add(new Thunfisch());
    }
}
