package factoryMethodPattern.zutaten;

public class Ananas extends Zutat {
    public Ananas() {
        super.name = "Ananas";
        super.preis = 0.5;
    }
}
