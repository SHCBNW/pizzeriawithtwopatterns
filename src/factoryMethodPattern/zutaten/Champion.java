package factoryMethodPattern.zutaten;

public class Champion extends Zutat {
    public Champion() {
        super.name = "Champion";
        super.preis = 0.5;
    }
}
