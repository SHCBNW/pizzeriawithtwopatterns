package factoryMethodPattern.zutaten;

public class Mais extends Zutat {
    public Mais() {
        super.name = "Champion";
        super.preis = 0.5;
    }
}
