package factoryMethodPattern.zutaten;

public class Salami extends Zutat {
    public Salami() {
        super.name = "Champion";
        super.preis = 0.5;
    }
}
