package factoryMethodPattern.zutaten;

public class Schinken extends Zutat {
    public Schinken() {
        super.name = "Schinken";
        super.preis = 0.5;
    }
}
