package factoryMethodPattern.zutaten;

public class Thunfisch extends Zutat {
    public Thunfisch() {
        super.name = "Champion";
        super.preis = 0.5;
    }
}
