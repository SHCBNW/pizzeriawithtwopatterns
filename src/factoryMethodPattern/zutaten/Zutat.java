package factoryMethodPattern.zutaten;

public abstract class Zutat {
    String name;
    double preis;

    public String getName() {
        return name;
    }

    public double getPreis() {
        return preis;
    }
}
