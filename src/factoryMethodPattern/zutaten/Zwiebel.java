package factoryMethodPattern.zutaten;

public class Zwiebel extends Zutat {
    public Zwiebel() {
        super.name = "Champion";
        super.preis = 0.5;
    }
}
